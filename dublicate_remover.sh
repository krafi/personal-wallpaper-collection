# Set the path to the directory
dir_path="./Wallpapers@krafi.info/"

# Get the list of all files in the directory
files=$(ls "$dir_path")

# Create an empty array to store file sizes
sizes=()

# Iterate through the files and store their sizes in the array
for file in $files; do
    file_path="$dir_path/$file"
    if [[ -f "$file_path" ]]; then
        size=$(du -b "$file_path" | cut -f1)
        if [[ " ${sizes[@]} " =~ " $size " ]]; then
            # If a file with the same size already exists, delete the current file
            rm "$file_path"
            echo "Deleted $file_path"
        else
            sizes+=("$size")
        fi
    fi
done
