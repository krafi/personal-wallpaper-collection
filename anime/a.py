
import Xlib
import Xlib.display as display
import subprocess

def lock_screen():
    # Create an X display instance
    d = display.Display()

    # Get the root window
    root = d.screen().root

    # Grab the keyboard and pointer to block input events
    root.grab_pointer(True, 3, Xlib.GrabModeAsync,
                      Xlib.GrabModeAsync, Xlib.CurrentTime)
    root.grab_keyboard(True, 3, Xlib.GrabModeAsync,
                       Xlib.GrabModeAsync, Xlib.CurrentTime)

    # Black out the screen using a subprocess call
    subprocess.call(['xset', 'dpms', 'force', 'off'])

    # Keep processing events until the lock is released
    while True:
        # Process pending X events
        event = d.next_event()

        # Uncomment the line below to print event information for debugging
        # print(event)

        # Handle specific events (e.g., key release) to unlock the screen
        if event.type == Xlib.X.KeyPress:
            break

    # Release the grab and unblank the screen
    root.ungrab_pointer(Xlib.CurrentTime)
    root.ungrab_keyboard(Xlib.CurrentTime)
    subprocess.call(['xset', 'dpms', 'force', 'on'])

if __name__ == "__main__":
    lock_screen()
