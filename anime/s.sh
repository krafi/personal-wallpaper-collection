#!/bin/bash

# Set the path to your wallpaper directory
WALLPAPER_DIR="/home/krafi.info/Pictures/personal-wallpaper-collection/anime"

# Get a random wallpaper from the directory
wallpaper=$(find "$WALLPAPER_DIR" -type f | shuf -n 1)

# Set the wallpaper using feh
feh --bg-fill "$wallpaper"

# Lock the screen using slock
slock
