#!/bin/bash

if [ -x "$(command -v i3)" ]; then
  if grep -q '^exec --no-startup-id ~/Pictures/personal-wallpaper-collection/./set_nitrogen.sh' ~/.config/i3/config; then
    echo "The exec command already exists in the i3 configuration file."
  else
    echo "Adding the exec command to the i3 configuration file..."
    echo 'exec --no-startup-id ~/Pictures/personal-wallpaper-collection/./set_nitrogen.sh' >> ~/.config/i3/config
  fi
else
  echo "i3 is not installed on your system."
fi

WALLPAPER_DIR=~/Pictures/personal-wallpaper-collection/Wallpapers@krafi.info
WALLPAPERS=( $(ls $WALLPAPER_DIR) )

# Initialize the variable to store the process ID of the previous Nitrogen instance
PREV_NITROGEN_PID=""

# Function to kill the previous Nitrogen process
kill_previous_nitrogen() {
  if [ -n "$PREV_NITROGEN_PID" ]; then
    echo "Killing previous Nitrogen process (PID: $PREV_NITROGEN_PID)..."
    kill $PREV_NITROGEN_PID
    PREV_NITROGEN_PID=""
  fi
}

# Function to check CPU usage
check_cpu_usage() {
  idle=$(mpstat 1 6 | awk '/Average/ {print $12}')
  cpu_usage=$(echo "100 - $idle" | bc)
  cpu_usage=$(printf "%.0f" "$cpu_usage")
  echo "$cpu_usage"
}

# Trap the script's termination signals to ensure the previous Nitrogen process is killed
trap 'kill_previous_nitrogen; exit' EXIT

# Loop forever
while true; do
  CPU_USAGE=$(check_cpu_usage)

  if [ "$CPU_USAGE" -lt 80 ]; then
    RANDOM_WALLPAPER=${WALLPAPERS[$RANDOM % ${#WALLPAPERS[@]}]}
    kill_previous_nitrogen
    nitrogen --set-zoom-fill $WALLPAPER_DIR/$RANDOM_WALLPAPER &
    PREV_NITROGEN_PID=$!
  else
    echo "CPU usage is too high ($CPU_USAGE%), skipping wallpaper change..."
  fi

  sleep 1
done
